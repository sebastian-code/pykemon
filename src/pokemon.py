__author__ = 'Ramon Caraballo'

import random

from src import atacar


class Pokemon(atacar.Atacar):
    def __init__(self):
        # self.hp = random.randint(15, 30)
        self.hp = 20
        self.defz = random.randint(1, 10)
        self.devil_a = self.devilidades(self.tipo)
        self.fuerte_a = self.fortalzas(self.tipo)

    def __str__(self):
        return """{pkm} HP:{hp}""".format(pkm=self.__class__.__name__,
                                          hp=self.hp
                                          )

    def devilidades(self, tipo):
        # TODO: Hacer que si el PKM es atacado por su debilidad se le reste mas puntos de HP
        """
        A: Agua devil a H
        F: Fuego devil a A
        H: Hierva devil a F
        """
        devilidad = {
            "A": "H",
            "F": "A",
            "H": "F",
        }

        return devilidad[tipo]

    def fortalzas(self, tipo):
        # TODO Hacer que si el PKM es atacado por su fortaleza se le reste menos puntos de HP
        """
        A: Agua fuerte a F
        F: Fuego fuerte a H
        H: Hierva fuerte a A
        """
        fortaleza = {
            "A": "F",
            "F": "H",
            "H": "A",
        }

        return fortaleza[tipo]
