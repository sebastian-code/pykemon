__author__ = 'Ramon Caraballo'

from src import pokemon

"""
Hay que sobre-escribir la variable atk para ortorgarles los ataques a los PKM

        COMO SE LE DEBE ASIGNAR LOS ATAQUES A LOS PKMN
----------------------------------------------------------------

atk = [
    {'ataque': {"atk_name": "nombre del ataq", "dano": 5, "usos": 5}},
    {'ataque': {"atk_name": "nombre del ataq", "dano": 2, "usos": 5}},
    {'ataque': {"atk_name": "nombre del ataq", "dano": 7, "usos": 3}},
    {'ataque': {"atk_name": "nombre del ataq", "dano": 2, "usos": 5}},
]
----------------------------------------------------------------

NATA: Se puede cambiar el dano del ataque y su nombre.

NOTA2: No modificar la cantidad de usos. Para asi mantener
        un mejor funcionamiento.


"""

# TODO hacer que los ataques tengan una propieda ejmplo: Fuego, Agua, Etc.


class Charmander(pokemon.Pokemon):
    # tipo Fuego
    tipo = 'F'

    atk = [
        {'ataque': {"atk_name": "Araniazo", "dano": 5, "usos": 5}},
        {'ataque': {"atk_name": "Ascuas", "dano": 6, "usos": 5}},
        {'ataque': {"atk_name": "Garra Metal", "dano": 7, "usos": 3}},
        {'ataque': {"atk_name": "Furia", "dano": 2, "usos": 5}}, 1
    ]


class Bulbasaur(pokemon.Pokemon):
    # tipo Hierva
    tipo = 'H'

    atk = [
        {'ataque': {"atk_name": "Placaje", "dano": 5, "usos": 5}},
        {'ataque': {"atk_name": "Hojas Navajas", "dano": 3, "usos": 5}},
        {'ataque': {"atk_name": "Doble Filo", "dano": 4, "usos": 3}},
        {'ataque': {"atk_name": "Rayo Solar", "dano": 7, "usos": 5}},
    ]


class Squirtle(pokemon.Pokemon):
    # tipo Agua
    tipo = 'A'

    atk = [
        {'ataque': {"atk_name": "Placaje", "dano": 5, "usos": 5}},
        {'ataque': {"atk_name": "Burbujas", "dano": 2, "usos": 5}},
        {'ataque': {"atk_name": "Pistola Agua", "dano": 7, "usos": 3}},
        {'ataque': {"atk_name": "Mordisco", "dano": 2, "usos": 5}},
    ]
