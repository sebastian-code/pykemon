__author__ = 'Ramon Caraballo'

import random


class Atacar(object):
    # TODO hacer que tome la propieda del ataque.
    # TODO hacer que tome las devilidades y las fortalza de los PKM.

    # lista de ataques que tiene ese pokemon.
    atk = list()


    def falla(self):
        # escoge un numero aleatorio de 1 al 10 y si es mayor que 7 el
        # Pokemon galla el ataque
        esquivar = random.randint(1, 10)

        # devuelve True si el numero es mayor que 7
        return esquivar > 7


    def critico(self):
        # saber si el ataque es critico
        crit = random.randint(1, 10)

        return crit > 7


    def atacar(self, maquina=False):

        atk_opc = """
        ----------------------------------------------------------------
                                ATAQUES:
        ----------------------------------------------------------------
            [1]{atk1} ({a}/5)           [2]{atk2} ({b}/5)

            [3]{atk3} ({c}/3)        [4]{atk4} ({d}/5)
            Cual Ataques elijes  [1, 2, 3 o 4] :
        ----------------------------------------------------------------
            """.format(a=self.atk[0]['ataque']['usos'],
                       b=self.atk[1]['ataque']['usos'],
                       c=self.atk[2]['ataque']['usos'],
                       d=self.atk[3]['ataque']['usos'],
                       atk1=self.atk[0]['ataque']['atk_name'],
                       atk2=self.atk[1]['ataque']['atk_name'],
                       atk3=self.atk[2]['ataque']['atk_name'],
                       atk4=self.atk[3]['ataque']['atk_name'],

                       )

        if maquina:
            # si es la maquina cogera un numero al azar.
            opc = random.choice('1234')

        else:
            print atk_opc
            opc = raw_input(">>: ")


        if opc in "1234":

            opc = int(opc) - 1


            # Si el ataque Selecionado tiene Puntos para usarse
            if self.atk[opc]['ataque']['usos']:

                self.atk[opc]['ataque']['usos'] -= 1

                # Trata de ver si
                if self.falla():

                    print '{} ha fallado el ataque.'.format(
                        self.__class__.__name__)

                    return 0


                print "{pkm} a usando {     atk}".format(
                    pkm= self.__class__.__name__,
                    atk= self.atk[opc]['ataque']['atk_name']
                )

                if self.critico():
                    # si es critico le suma 3 puntos al dano
                    print 'Ohh ah sido un Golpe Critico.'
                    return self.atk[opc]['ataque']['dano'] + 3

                # de lo contrario le pega normal.
                return self.atk[opc]['ataque']['dano']

            else:
                print 'Ya este ataque no tiene mas P/P'
                self.atacar()

        else:
            # Si no escoge entre los numero 1, 2, 3, 4 vuelve y ejecuta
            #  el metodo atacar
            self.atacar()
