"""
__author__ = 'Ramon Caraballo'
__version__ = "0.1.0"
"""
import sys
import time

from src.pick_one import Bulbasaur
from src.pick_one import Charmander
from src.pick_one import Squirtle


class Game:
    def pantalla(self):
        print """
        -----------------------------------------------------------------
        |{mkn}/20
        |{mkhp}
        |------------------------                                        |
        |                                                                |
        |                                                                |
        |                                                                |
        |                                        ------------------------|
        |                                       |TU | {pj}/20
        |                                       |{pjhp}
        -----------------------------------------------------------------|

        """.format(mkn=self.maquina, mkhp='=' * self.maquina.hp,
                   pj=self.pj, pjhp='=' * self.pj.hp
                   )

    def setup(self):
        self.pkm = {1: Charmander(), 2: Bulbasaur(), 3: Squirtle()}
        print """
                   --= Bienvenidos a Pokemon-Py =--

              Seleciona con que Pokemon te gustaria jugar:
            [1] Charmander    [2] Bulbasaur    [3] Squirtle"""

        opc = raw_input('1, 2 o 3 ?\n>>: ')

        if opc in "123":
            try:
                opc = int(opc)

                self.pj = self.pkm[opc]

            except KeyError:
                self.setup()

            # self.maquina = self.pkm[random.randint(1,3)]


            try:
                # Opcion style PKM donde tu Rival escoge lo que le gana a tu
                #  PKM.

                # Ej: si Cojes a Charmander tu rival escoge a Squirtle.
                self.maquina = self.pkm[opc - 1]

            except KeyError:
                self.maquina = self.pkm[3]

        else:
            self.setup()


    def turno_maquina(self):
        print "Turno de la Maquina...\nPensando . . ."
        time.sleep(7)

        dmg = self.maquina.atacar(maquina=True)

        self.pj.hp -= dmg

        if self.pj.hp <= 0:
            print "Has perdido Tu {}\
                   no Puede Continuar".format(self.pj.__class__.__name__)
            sys.exit()


    def turno_pj(self):
        print"""
        ----------------------------------------------------------------
        |                         Tu Turno                             |
        ----------------------------------------------------------------
        |    [A]tacar                               [?]?????           |
        |                                                              |
        |                                                              |
        |    [?]?????                               [S]alir            |
        ---------------------------------------------------------------
        """
        pj_opc = raw_input('>>: ').lower()

        if pj_opc in 'as':

            if pj_opc == 'a':
                dmg = self.pj.atacar()

                self.maquina.hp -= dmg

                if self.maquina.hp <= 0:
                    # Imprime el Nombre del Pokemon.
                    print "{} No puede continuar" \
                        .format(self.maquina.__class__.__name__)

                    print "Has Ganado!!"

                    sys.exit()

            elif pj_opc == 's':
                sys.exit()

        else:
            self.turno_pj()


if __name__ == '__main__':
    game = Game()
    game.setup()

    while True:
        game.pantalla()
        game.turno_pj()
        game.pantalla()
        game.turno_maquina()
